import random
import numpy as np
import os
import difflib
import sys
'''
Prereqs:
Input is formatted like:
----------start
rA cA
rB cB

[Matrix A]

[Matrix B]
----------end
where 
rA: row count of matrix A
cA: column count of matrix A
rB: row count of matrix B
cB: column count of matrix B
[Matrix A]: matrix elements seperated by spaces of dim rA x cA
[Matrix B]: matrix elements seperated by spaces of dim rB x cB

Output is formatted like:
----start
[matrix C]
----end
where 
[matrix C]: matrix elements seperated by spaces of dim rA x cB


<<<Confused?>>>
Run the script once to have some random input files generated that you can inspect.

How to use:

python test.py 2 10

To run 10 iterations of random testing on 2 nodes

python test.py 0 100

To run 100 iterations of random testing on a random number of nodes between 0 and 10

'''
procs = int(sys.argv[1])
iterationsToRun = int(sys.argv[2])
for iters in range(iterationsToRun):
    if procs == 0:
        procs = random.randint(1,10)
    runCmd = "cat input.txt | mpirun -n " + str(procs) + " a.out > output.txt"
    dims = [random.randint(1, 3) for x in range(3)]
    X = np.random.randint(100, size=(dims[0], dims[1]))
    Y = np.random.randint(100, size=(dims[1], dims[2]))
    res = np.dot(X,Y)
    inFile = open("input.txt", "w")
    inFile.write(str(dims[0]) + " " + str(dims[1]) + "\n")
    inFile.write(str(dims[1]) + " " + str(dims[2]) + "\n")

    for row in X:
        inFile.write(' '.join([str(col) for col in row]))
        inFile.write("\n")
    inFile.write("\n")
    
    for row in Y:
        inFile.write(' '.join([str(col) for col in row]))
        inFile.write("\n")
    inFile.close()

    os.system(runCmd)
    outFile = open("output.txt", "r")

    correctOutFile = open("correctOutput.txt", "w")
    for row in res:
        correctOutFile.write(' '.join([str(col) for col in row]))
        correctOutFile.write("\n")
    correctOutFile.close()
    
    with open('correctOutput.txt') as c:
        cOutMat = [line.split() for line in c.readlines()]
    with open('output.txt') as o:
        outMat = [line.split() for line in o.readlines()]
    for i in range(dims[0]):
        for j in range(dims[2]):
            if int(cOutMat[i][j]) != int(float(outMat[i][j])):
                print("Didn't work, check the input/output files")
                break
    print("Worked for iteration: "+str(iters))
