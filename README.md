*Prereqs:*  
Input is formatted like:  
----------start  
rA cA  
rB cB  

[Matrix A]  

[Matrix B]  
----------end  
where  
rA: row count of matrix A  
cA: column count of matrix A  
rB: row count of matrix B  
cB: column count of matrix B  
[Matrix A]: matrix elements seperated by spaces of dim rA x cA  
[Matrix B]: matrix elements seperated by spaces of dim rB x cB  

Output is formatted like:  
----start  
[matrix C]  
----end  
where   
[matrix C]: matrix elements seperated by spaces of dim rA x cB  


*Confused?*  
Run the script once to have some random input files generated that you can inspect.  

*How to use:*  


To run 10 iterations of random testing on 2 nodes  

`python test.py 2 10`  

To run 100 iterations of random testing on a random number of nodes between 0 and 10  

`python test.py 0 100` 


